package br.com.criandoapi.projeto.services;

import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.criandoapi.projeto.dto.UsuarioDTO;
import br.com.criandoapi.projeto.dto.UsuarioInsertDTO;
import br.com.criandoapi.projeto.entities.Usuario;
import br.com.criandoapi.projeto.repositories.UsuarioRepository;

@Service
public class UsuarioService {

	final UsuarioRepository usuarioRepository;
	final ModelMapper mapper;

	public UsuarioService(UsuarioRepository usuarioRepository, ModelMapper mapper) {
		this.usuarioRepository = usuarioRepository;
		this.mapper = mapper;
	}

	public Page<UsuarioDTO> buscarTodos(Pageable pageable) {
		Page<Usuario> page = usuarioRepository.buscarTodos(pageable);
		return page.map(x -> mapper.map(x, UsuarioDTO.class));
	}

	public UsuarioDTO inserir(UsuarioInsertDTO dto) {
		Usuario entity = mapper.map(dto, Usuario.class);
		entity = usuarioRepository.save(entity);
		return mapper.map(entity, UsuarioDTO.class);
	}

	public UsuarioDTO atualizar(Integer id, UsuarioInsertDTO dto) {
		Optional<Usuario> obj = usuarioRepository.findById(id);

		if (obj.isPresent()) {
			Usuario entity = obj.get();
			entity.setEmail(dto.getEmail());
			entity.setNome(dto.getNome());
			entity.setTelefone(dto.getTelefone());
			entity.setSenha(dto.getSenha());

			entity = usuarioRepository.save(entity);
			return mapper.map(entity, UsuarioDTO.class);
		}

		throw new IllegalArgumentException("Não existe o usuario com id " + id);
	}

	public UsuarioDTO buscar(Integer id) {
		Optional<Usuario> obj = usuarioRepository.buscar(id);

		return mapper.map(obj.get(), UsuarioDTO.class);
	}

	public ResponseEntity<String> remover(Integer id) {
		Optional<Usuario> obj = usuarioRepository.buscar(id);

		if (obj.isPresent()) {
			Usuario entity = obj.get();
			usuarioRepository.delete(entity);
			return ResponseEntity.status(HttpStatus.ACCEPTED).body("Usuário com id " + id + " removido");
		}

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Erro o usuário com id " + id + " não existe");
	}

}
