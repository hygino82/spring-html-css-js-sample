package br.com.criandoapi.projeto.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.criandoapi.projeto.entities.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

	@Query("SELECT obj FROM Usuario obj")
	Page<Usuario> buscarTodos(Pageable pageable);

	@Query("SELECT obj FROM Usuario obj WHERE obj.id = :id")
	Optional<Usuario> buscar(Integer id);

}
