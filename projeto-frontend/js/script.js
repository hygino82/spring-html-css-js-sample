const formulario = document.querySelector('form');
const inputNome = document.querySelector('.nome');
const inputEmail = document.querySelector('.email');
const inputSenha = document.querySelector('.senha');
const inputTel = document.querySelector('.tel');

function cadastrar() {

    data = {
        nome: inputNome.value,
        senha: inputSenha.value,
        email: inputEmail.value,
        telefone: inputTel.value
    }


    fetch('http://localhost:8080/api/usuario', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8', 'Accept': 'application/json',
        }
    })
        .then(response => response.json())
        .then(json => console.log(json))
        .catch(err => console.log(err));
}

function limpar() {
    inputNome.value = '';
    inputSenha.value = '';
    inputEmail.value = '';
    inputTel.value = '';
}

formulario.addEventListener('submit', function (event) {
    event.preventDefault();
    cadastrar();
    limpar();
});